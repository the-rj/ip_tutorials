<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Result extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();  
        $this->load->Model('Query');
    }
    public function index()
    {
        $this->load->view('login');
    }
    public function back()
    {
        //cancel button back to session.
        redirect(base_url().'Marksheet/index');
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');
        
        if ($this->form_validation->run()==false) {
            // redirect('');
            $this->load->view('login');
        }
        else{
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
            $this->load->model('Query');
            $login = $this->Query->login($data);
            if ($login) 
            { //create session...
                $data1 = array();
                $data1['id'] = $login->id;
                $data1['name'] = $login->first_name;
                $data1['email']= $this->input->post('email');
                $data1['password']= $this->input->post('password');
                $this->session->set_userdata($data1);
                if(!empty($this->input->post("remember"))) {
                    setcookie ("loginId", $data1['email'], time()+ (365 * 24 * 60 * 60));  
                    setcookie ("loginPass", $data1['password'],  time()+ (365 * 24 * 60 * 60));
                  } else {
                    setcookie ("loginId",""); 
                    setcookie ("loginPass","");
                  }       
                // $this->load->model('Query');
                // $data1['RK']=$this->Query->grade($data1);
                // $this->load->view('logout',$data1);
                redirect(base_url('Marksheet'));
            } else {
                $this->session->set_flashdata('fail_login', 'Invalid Credentials.');
                return redirect('result');
            }
        } 
    }
    public function lout()
    {
        $this->session->sess_destroy();
        redirect(base_url('Result'));
    }

    //for registration into user table "signing in".
    public function signup($id=0)
    {
        //used for adminpanel
        $data= array();
        $data['id'] = "";
        $data['first_name'] = "";
        $data['last_name'] = "";
        $data['email'] = "";
        $data['password'] = "";
        $data['phone'] = "";
        $data['profile_photo'] = "";
        
        $data1=array();
        $data1['upu']=$data; //upu=update user..
        if($id==0){
            $this->load->view('signup',$data1);
         }
         else{
            $this->load->model('Query');
            $member['up'] = $this->Query->euser($id); //up=update..
            // print_r($member['up']);
            $this->load->view('signup',$member);
         }
    }
    public function insert()
    {
        $data=array();
        $data['id']=$this->input->post('id');
        $data['first_name'] = $this->input->post('firstname');
        $data['last_name'] = $this->input->post('lastname');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['phone'] = $this->input->post('phone');
        $data['profile_photo'] = $this->image();
        $this->load->model('Query');   
        $this->Query->saveData($data);
        $this->session->set_flashdata('insert', 'Registerd Successfully.');
        redirect(base_url().'Result/signup');
    }
    private function image()
      {
         if(isset($_FILES['image']))
			{
				$uploaddir = 'uploads/'.uniqid(rand());
				$uploadfile = $uploaddir . basename($_FILES['image']['name']);
			
					if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                        return $uploadfile;
    					echo "File is valid, and was successfully uploaded.\n";
					} else {
    					echo "Possible file upload attack!\n";
               }
         }
      }

    //deletes marksheet row...
    public function delete($id)
    {         
        $this->load->model('Query');
        $this->Query->delete($id);
        redirect(base_url().'Marksheet/index');
    }

}
?>