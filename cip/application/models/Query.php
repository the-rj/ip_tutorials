<?php  
   class Query extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function login($data)  
      {  
         //data is retrive from this query  
         $query=$this->db->select('*')->from('users')->where(['email'=>$data['email'],'password'=>$data['password']])->get();  
         return $query->row();        
      }
      public function saveData($data)
      {
         if($data['id']==0)
         {
            $this->db->insert('users',$data);
         }else{
            $this->db->where('id', $data['id']);
            $this->db->update('users',$data);
         }
      }
      public function grade($data)
      {
         $query=$this->db->SELECT('users.profile_photo,users.phone,results.image,results.id,results.email,results.en_no, results.fullname, results.semester, results.field, results.SGPA, results.CGPA')->FROM('results')->JOIN('users','users.email=results.email')->WHERE('results.email',$data['email'])->get();
         if($query->num_rows()>0)
          return $query->result_array();
         else{
            $error['status']="Record Not Found";
            return $error;
         }
      }
      
      public function delete($id)
      {
         $this->db->where('id',$id)->delete('results');
      }
      public function markData($data)
      {
         if($data['id']==0){
            $this->db->insert('results',$data);
         }else{
            $this->db->where('id', $data['id']);
            $this->db->update('results',$data);
         }
      }
      public function edit($id)
      {
         $this->db->where('id',$id);
         return $member=$this->db->get('results')->result_array()[0];
      }

   }  
?>