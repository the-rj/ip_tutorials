  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  
    <a class="navbar-brand ml-2" href="https://www.rku.ac.in/"><img src="<?= base_url('/assets/logo.png'); ?>" height="35px" width="65px"> RK University</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarColor02">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="#">Syllabus</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="http://erp.rku.ac.in/RKIMSSTUD/frmLogin.aspx">ERP</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://canvas.rku.ac.in/">Canvas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="http://library.rku.ac.in/webopac/index.php">Library</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-danger btn-sm mt-1 mr-2" accesskey="l" href="<?= base_url('Result/lout'); ?>" onClick="return confirm('Do you want to logout?');">Logout</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" name="name" id="name" placeholder="Search">
          <button type="submit" class="btn btn-light">Search</button>
      </form>
    </div>
  </nav>