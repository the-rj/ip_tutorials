<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html>

<head>
    <title>Registration Form</title>
    <link rel="icon" href="<?= base_url(); ?>assets/sicon.PNG" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url('/assets/css/bootstrap.min.css') ?>" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('/assets/css/my.css') ?>" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body class="bg-primary">
    <div class="container card text-white bg-primary center-block head mt-5" style="max-width: 22rem;">
        <br>
        <?php if ($error = $this->session->flashdata('success')) {  ?>
            <p class="text-danger"><?= $error; ?></p>
        <?php } ?>
        <h1 class="text-white text-center">Register Here!</h1>
        <form method="post" action="<?= base_url('Result/insert') ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo set_value('id', $upu['id']) ?>" />
            <fieldset>
                <legend class="text-danger">College student only!!</legend>
                <div class="form-group">
                    <label class="form-control-label" for="inputSuccess1">Firstname<span class="required">*</span></label>
                    <input type="text" placeholder="Your firstname . . ." class="form-control" name="firstname" id="inputValid" value="" />
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="inputSuccess1">Lastname<span class="required">*</span></label>
                    <input type="text" placeholder="Your lastname . . ." class="form-control" name="lastname" id="inputValid" value="" />
                </div>
                <div class="form-group">
                    <label for="InputEmail">Email address<span class="required">*</span></label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="Enter your email . . ." value="" required />
                    <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="InputPassword">Password<span class="required">*</span></label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Your Password . . ." value="" required />
                    <?= form_error('password', "<p class='text-danger'>", "</p>"); ?>
                </div>
                <div class="form-group">
                    <label>Mobile Number<span class="required">*</span></label>
                    <input type="number" class="form-control" value="" id="phone" placeholder="Enter Your Mobile Number" name="phone">
                    <span class="hidden" id="moberr"></span>
                </div>
                <div class="form-group">
                    <label>Image<span class="required">*</span></label>
                    <div class="custom-file">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                        <input type="file"  class="custom-file-input" id="image" placeholder="Select Image" name="image">
                    </div>
                </div>
                <button type="submit" class="btn btn-warning btn-outline-primary btn-sm">Signup</button>
                <?php echo anchor('Result/back', 'Login',['class' => 'btn btn-danger btn-outline-primary btn-sm','accesskey'=>'c']);?>
            </fieldset>
        </form>
    </div>
</body>

</html>
<!-- Java Script -->
<script src="<?= base_url('/assets/js/jquery.min.js') ?>"></script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<!-- Java Script -->
