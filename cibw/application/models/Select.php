<?php  
   class Select extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function add()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('members');  
         return $query;  
      }  
      public function saveData($data)
      {
         if($data['id']==0)
         {
            $this->db->insert('members',$data);
         }else{
            $this->db->where('id', $data['id']);
            $this->db->update('members',$data);
         }
      }
      public function duser($id)
      {
         $this->db->where('id',$id)->delete('members');
      }
      public function euser($id)
      {
         $this->db->where('id',$id);
         return $member=$this->db->get('members')->result_array()[0];
      }
      
   }  
?>