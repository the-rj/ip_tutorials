<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html>

<head>
    <title>Registration Form</title>
    <link rel="icon" href="<?= base_url(); ?>assets/sicon.PNG" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url('/assets/css/bootstrap.min.css') ?>" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('/assets/css/my.css') ?>" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .round {
            border-radius: 20px;
        }
    </style>
</head>
<!-- <?php
        echo "<pre>";
        print_r($m);
        echo "</pre>";
        ?> -->

<body class="bg-primary">
    <div class="container card text-white bg-primary center-block head mt-5" style="max-width: 22rem;">
        <br>
        <?php if ($error = $this->session->flashdata('entered')) {  ?>
            <p class="text-danger"><?= $error; ?></p>
        <?php } ?>
        <h2 class="text-white text-center"><font color="red">~>></font>RKU Result<font color="red"><<~</font> </h2> 
        <form name="score" id="score" method="post" action="<?= base_url('Marksheet/save') ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo set_value('id', $m['id']) ?>" />
            <fieldset>
                <legend class="text-danger">Enter Valid Details!!</legend>
                <div class="form-group">
                    <label class="form-control-label" for="inputSuccess1">UID</label>
                    <input type="text" style="border-radius:10px;" placeholder="Enrollment . . ." class="form-control" name="uid" id="inputValid" value="<?php echo set_value('uid', $m['en_no']); ?>" required />
                    <small class="form-text text-muted">Go for the new One.</small>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="inputSuccess1">Fullname</label>
                    <input type="text" style="border-radius:10px;" placeholder="Enter fullname . . ." class="form-control" name="fullname" id="inputValid" value="<?php echo set_value('fullname', $m['fullname']); ?>" />
                    <small class="form-text text-muted">As per the one's SSC Result.</small>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Email address</label>
                    <input type="email" style="border-radius:10px;" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="Enter email address . . ." value="<?php echo set_value('email', $m['email']); ?>" required />
                    <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="course">Field</label>
                    <input type="text" style="border-radius:10px;" class="form-control" id="btech" name="btech" placeholder="Stream of education/Discipline/Department . . ." value="<?php echo set_value('btech', $m['field']); ?>" />
                </div>
                <div class="form-group">
                    <label for="sem">Semester</label>
                    <input type="text" style="border-radius:10px;" class="form-control" id="sem" name="sem" placeholder="Enter the semester in words . . ." value="<?php echo set_value('sem', $m['semester']); ?>" />
                </div>
                <div class="form-group">
                    <label for="SGPA">SGPA</label>
                    <input type="text" style="border-radius:10px;" class="form-control" id="sgpa" name="sgpa" placeholder="Enter Sem Grade Point Average . . ." value="<?php echo set_value('sgpa', $m['SGPA']); ?>" />
                </div>
                <div class="form-group">
                    <label for="course">CGPA</label>
                    <input type="text" style="border-radius:10px;" class="form-control" id="cgpa" name="cgpa" placeholder="Enter Cumutilitve Grade Point Average . . ." value="<?php echo set_value('cgpa', $m['CGPA']); ?>" />
                </div>
                <div class="form-group">
                    <label>Image<span class="required">*</span></label>
                    <div class="custom-file">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                        <input type="file"  class="custom-file-input" id="image" placeholder="Select Image" name="image">
                    </div>
                    <small class="form-text text-muted">Select image of your building.</small>
                </div>
                <button type="submit" class="btn round btn-info btn-sm" value="upload">Submit</button>
                <button type="reset" class="btn round btn-danger btn-sm">Reset</button>
                <?php echo anchor('Result/back', 'Cancel', ['class' => 'btn round btn-warning btn-sm', 'accesskey' => 'c']); ?>
            </fieldset>
        </form>
    </div>
</body>

</html>
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>