$("document").ready(function() {
    $("#email").blur(function() {
        if (!$("#email").val()) {
            $("#usererr").html("Please Enter Username");
            $("#usererr").css("color", "red");
            $("#email").removeClass('shadow-success').addClass("shadow-danger");
            $("#usererr").removeClass("hidden");
            // alert("hello"); 
        }
        if ($("#email").val()) {
            var uname1 = $("#email").val();
            var uname = /^(?=.*\d)(?=.*[A-Za-z]).{2,20}$/;
            if (uname.test(uname1)) {
                if (uname1.length <= 4) {
                    // alert("test block");
                    $("#usererr").html("Username must be greater than 4 characters");
                    $("#usererr").css("color", "red");
                    $("#email").val("");
                    $("#email").css("border-color", "red");
                    $("#email").removeClass('shadow-success').addClass("shadow-danger");
                    $("#usererr").removeClass("hidden");

                } else {
                    $("#usererr").html("");
                    $("#usererr").css("color", "green");
                    $("#email").removeClass('shadow-danger').addClass("shadow-success");
                    $("#usererr").addClass("hidden");

                }
            } else {
                $("#email").val("");
                $("#email").focus();
                $("#usererr").html("Username must contain alphabets and numbers");
                $("#usererr").css("color", "red");
                $("#email").removeClass('shadow-success').addClass("shadow-danger");
                $("#usererr").removeClass("hidden");
            }
            //alert("hello");
        }
    });
});