<?php  
   class Clubuv extends CI_Controller  
   {  
      public function index()  
      {  
         //load the database  
         $this->load->database();  
         //load the model  
         $this->load->model('Select');  
         //load the method of model  
         $data['R']=$this->Select->add();  
         //return the data in view  
         $this->load->view('club', $data);  
      } 

      public function form($id=0)
      {
         $data= array();
         $data['id'] = "";
         $data['fullname'] = "";
         $data['firstname'] = "";
         $data['lastname'] = "";
         $data['email'] = "";
         $data['password'] = "";
         $data['contact'] = "";
         $data['birthday'] = "";
         $data['gender'] = "";
         $data['country'] = "";
         $data['passno'] = "";
         $data['registerby'] = "";
         $data['status'] = "";
         $data['photo'] = "";

         $data1=array();
         $data1['J']=$data;
         if($id==0){
         $this->load->view('rform',$data1);
         
         }
         else{
            $this->load->model('Select');
            $member['J'] = $this->Select->euser($id);
            // print_r($member['J']);
            $this->load->view('rform',$member);
         }  
      }

      public function back()
      {
         redirect(base_url().'Clubuv/index');
      }

      public function save()
      {
            $data=array();
            $data['id']=$this->input->post('id');
            $data['fullname'] = $this->input->post('fullname');
            $data['firstname'] = $this->input->post('fname');
            $data['lastname'] = $this->input->post('lname');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('pass');
            $data['contact'] = $this->input->post('phone');
            $data['birthday'] = $this->input->post('bdate');
            $data['gender'] = $this->input->post('customRadio');
            $data['passno'] = $this->input->post('pn');
            $data['country'] = $this->input->post('con');
            $data['registerby'] = $this->input->post('rby');
            $data['status'] = $this->input->post('sat');
            $data['photo'] = $this->image();
            $this->load->model('Select');   
            $this->Select->saveData($data);
            redirect(base_url().'Clubuv/index');
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
      }

      private function image()
      {
         if(isset($_FILES['img']))
			{
				$uploaddir = 'uploads/'.uniqid(rand());
				$uploadfile = $uploaddir . basename($_FILES['img']['name']);
			
					if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                  return $uploadfile;
    					echo "File is valid, and was successfully uploaded.\n";
					} else {
    					echo "Possible file upload attack!\n";
               }
         }
      }

      public function delete($id)
      {         
         $this->load->model('Select');
         $this->Select->duser($id);
         redirect(base_url().'Clubuv/index');
      }
      
   }  
?>