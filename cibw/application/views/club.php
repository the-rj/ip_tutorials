<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Club Members</title>
        <link rel="icon" href="<?=base_url();?>assets/icon.PNG" type="image/x-icon">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/my.css" type="text/css" media="all">
        <script src="<?=base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
        <script>
        $(document).ready(function() {
            $("#name").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    </head>
    <body>
    <?php include_once("nav.php"); ?>
    <br>
    
    <div class="container text-white border-success mb-3">
        <div class="row">
            <div class="col-lg-10">
                <h3 class="text-warning">Members Detail</h3>
            </div>
            <div class="col-lg-2">
                <a href="<?=base_url('Clubuv/form')?>" class="btn btn-warning btn-lg btn-block">Insert</a>
            </div>
        </div>
    <table class="table head table-hover" border="2">
  <thead>
  <tbody>
    <tr>
        <th scope="col">Sr No.</th>
        <th scope="col">Image</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Password</th>
        <th scope="col">Contact</th>
        <th scope="col">Birthday</th>
        <th scope="col">Gender</th>
        <th scope="col">Country</th>
        <th scope="col">Passno</th>
        <th scope="col">Registerby</th>  
        <th scope="col">Status</th>
        <th scope="col">Action</th>
    </tr>
  </tbody>
    <tbody id="myTable">
    <?php
        $count=0;
        foreach($R->result() as $row)
        {
            ?>
    <tr>
        <td><?php echo $count +=1;?></td>
        <td><img src="<?=base_url();?>/<?=$row->photo;?>" height="50px" width="50px"/></td>
        <td><?=$row->fullname;?></td>
        <td><?=$row->email;?></td>
        <td><?=$row->password;?></td>
        <td><?=$row->contact;?></td>
        <td><?=$row->birthday;?></td>
        <td><?=$row->gender;?></td>
        <td><?=$row->country;?></td>
        <td><?=$row->passno;?></td>
        <td><?=$row->registerby;?></td>
        <td><?=$row->status;?></td>
        <td>
            <h5><a class="badge badge-primary" href="<?=base_url('Clubuv/form/').$row->id?>">Edit</a>
            <h5><a class="badge badge-danger" href="<?=base_url('Clubuv/delete/').$row->id?>" onClick="return confirm('Do you want to delete?');">Delete</a></h5>
        </td>
    </tr>
        <?php 
        }
        ?>
  </tbody>
  </thead>
</table> 
</div>
    </body>    
</html>