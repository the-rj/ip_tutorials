<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html>

<head>
    <title>Login Form</title>
    <link rel="icon" href="<?= base_url(); ?>assets/licon.PNG" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url('/assets/css/bootstrap.min.css') ?>" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('/assets/css/my.css') ?>" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body class="bg-primary">
    <div class="container card text-white bg-primary center-block head mt-5" style="max-width: 22rem;">
        <br>
        <?php if ($error = $this->session->flashdata('fail_login')) {  ?>
            <div class="text-danger">
                <?= $error; ?>
            </div>
        <?php } ?>
        <h1 class="text-white text-center">Login Here!</h1>
        <form method="post" action="<?= base_url('Result/login') ?>">
            <fieldset>
                <legend class="text-danger">College User</legend>
                <div class="form-group">
                    <label for="InputEmail">Email address</label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="Enter your email . . ." value="<?php if (isset($_COOKIE["loginId"])) {echo $_COOKIE["loginId"];} ?>">
                    <?= form_error('email', "<p class='text-danger'>", "</p>"); ?>
                    <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Your Password . . ." value="<?php if (isset($_COOKIE["loginPass"])) {echo $_COOKIE["loginPass"];} ?>">
                    <?= form_error('password', "<p class='text-danger'>", "</p>"); ?>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1"  name="remember" <?php if (isset($_COOKIE["loginId"])) { ?> checked="checked" <?php }; ?> >
                        <label class="custom-control-label" for="customCheck1">Remember Me</label>
                    </div>
                </div>
                <button type="submit" accesskey='s' class="btn btn-light btn-outline-dark btn-sm">Submit</button>
                <a href="<?=base_url('Result/signup')?>"><button type="button" class="btn btn-secondary btn-outline-primary btn-sm">Signup</button></a>
            </fieldset>
        </form>
    </div>
</body>

</html>
