<?php
class Info extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check login status
        if (!isset($this->session->userdata['email'])){
            redirect(base_url('Users'));
        }
        if (isset($_COOKIE['logincount'])) {
            setcookie('logincount', $_COOKIE['logincount'] + 1, time() + (60 * 60 * 24 * 30));
        } else {
            setcookie('logincount', 1, time() + (60 * 60 * 24 * 30));
        }
        $this->load->Model('Query');
    }
    public function index()
    {
        $data = array();
        $data['email'] = $this->session->userdata['email'];
        $data['password'] = $this->session->userdata['password'];
        $data = $this->Query->login($data);
        if ($data['user_type'] == 'admin') {

            redirect(base_url('Users/admin'));
        } else {
            $data=array();
            $data['email']=$this->session->userdata['email'];
            $data['RK']=$this->Query->grade($data);
            $this->load->view('dashboard',$data);
        }

    }
    
    public function details($id=0)
    {
        $data=array();
        $data['id']="";
        $data['en_no']="";
        $data['fullname'] = "";
        $data['email'] = "";
        $data['field'] = "";
        $data['year'] = "";
        $data['phone'] = "";
        $data['image'] = "Chosse File";

        $data1=array();
        $data1['m']=$data;
        if($id==0){
            $this->load->view('details',$data1);
        }
        else{
            $this->load->model('Query');
            $member['m'] = $this->Query->edit($id);
            // print_r($member['J']);
            $this->load->view('details',$member);
        }
    }
    public function save()
    {
        $data=array();
        $data['id']=$this->input->post('id');
        $data['en_no'] = $this->input->post('uid');
        $data['fullname'] = $this->input->post('fullname');
        $data['email'] = $this->input->post('email');
        $data['field'] = $this->input->post('btech');
        $data['year'] = $this->input->post('sem');
        $data['phone'] = $this->input->post('phone');
        $data['image'] = $this->image();
        $this->load->model('Query');   
        $this->Query->markData($data);
        $this->session->set_flashdata('entered', 'Data Successfully Inserted.');
        redirect(base_url().'Info/details');
    }

    private function image()
    {
        if(isset($_FILES['image']))
            {
                $uploaddir = 'uploads/'.uniqid(rand()).'.';
                $uploadfile = $uploaddir . basename($_FILES['image']['name']);
            
                    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                return $uploadfile;
                        echo "File is valid, and was successfully uploaded.\n";
                    } else {
                        echo "Possible file upload attack!\n";
            }
        }
    }

}
?>