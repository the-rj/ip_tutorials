<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html>

<head>
    <title>Score-board</title>
    <link rel="icon" href="<?= base_url(); ?>/assets/micon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url('/assets/css/bootstrap2.min.css'); ?>" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('/assets/css/my.css') ?>" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->load->view('nav.php'); ?>
</head>

<body>
    <br>
    <div class="container text-white border-success mb-3">
        <div class="row">
            <div class="col-sm-10">
                <h4 class="text-warning">Semesterwise Score</h4>
            </div>
            <div class="col-lg-2">
                <a accesskey="i" href="<?= base_url('Marksheet/score') ?>" class="btn btn-warning btn-lg btn-block">Insert</a>
            </div>
        </div>
        <h2 class="text-danger text-center">
            <marquee>
                <?php 
                // echo "<pre>";
                //     print_r($RK);
                // echo "</pre>";
                if(isset($RK['status'])){echo $RK['status'];} else{?> 
            </marquee>
        </h2>
        <br>
        <div class="row" id="search">
            <?php foreach($RK as $row){ ?>
            <div class="card text-white shade" id="1">
                <img class="card-img-top img" src="<?= base_url(); ?>/<?= $row['profile_photo']; ?>" height="190px" width="130px" />
                <div class="card-body">
                    <p class="card-text" style="margin-top: -13px">Full Name--><?= $row['en_no']; ?></p>
                    <p class="card-text" style="margin-top: -13px">Email--><?= $row['email']; ?></p>
                    <p class="card-text" style="margin-top: -13px">Enrollment No.--><?= $row['fullname']; ?></p>
                    <p class="card-text" style="margin-top: -13px">Mobile No.--><?= $row['phone'];?></p>
                    <p class="card-text" style="margin-top: -13px">Field<?= $row['field']; ?></p>
                    <a class="btn btn-success btn-sm mr-1" style="margin-top: -13px" href="<?= base_url('Marksheet/score/') . $row['id'] ?>">Edit</a>
                    <a class="btn btn-danger btn-sm ml-1" style="margin-top: -13px" href="<?= base_url('Result/delete/') . $row['id'] ?>" onClick="return confirm('Do you want to delete?');">Delete</a>
                </div>
            </div>
            <?php break; } ?>
            <?php foreach ($RK as $row) { ?>
                <div class="card text-white shade" id="1">
                    <img class="card-img-top img" src="<?= base_url(); ?>/<?= $row['image']; ?>" height="190px" width="130px" />
                    <div class="card-body">
                        <p class="card-text" align="center">
                            <legend style="margin-top: -15px"><?= $row['semester']; ?> Semester</legend>
                        </p>
                        <p class="card-text">SGPA--><?= $row['SGPA']; ?></p>
                        <p class="card-text">CGPA--><?= $row['CGPA']; ?></p>
                        <a class="btn btn-success btn-sm mr-1" href="<?= base_url('Marksheet/score/') . $row['id'] ?>">Edit</a>
                        <a class="btn btn-danger btn-sm ml-1" href="<?= base_url('Result/delete/') . $row['id'] ?>" onClick="return confirm('Do you want to delete?');">Delete</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>
<script src="<?= base_url(); ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $("#name").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#search #1").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
<?php }?>