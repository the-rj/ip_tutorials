<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Club UV</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="container">
  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Gallery</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('Clubuv/index')?>">Members<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">User</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="#">Vote</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('Player/index')?>">Winners</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" id="name" placeholder="Search">
      <input type="submit" class="btn btn-light my-2 my-sm-0" value="Search" />
    </form>
  </div>
</div>
</nav>