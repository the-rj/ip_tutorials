<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check login status
        $this->load->database();
        $this->load->Model('Query');
    }
    public function index()
    {
        $this->load->view('login');
    }
    public function back()
    {
        //cancel button back to session.
        $data = array();
        $data['email'] = $this->session->userdata['email'];
        $data['password'] = $this->session->userdata['password'];
        $data = $this->Query->login($data);
        // echo "<pre>";   
        //     print_r($data);
        // echo "<pre>";
        if ($data['user_type'] == 'admin') {

            redirect(base_url('Users/admin'));
        } else {

            redirect(base_url() . 'info/index');
        }
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() == false) {
            // redirect('');
            $this->load->view('login');
        } else {
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
            $this->load->model('Query');
            $login = $this->Query->login($data);
            // echo "<pre>";
            //         print_r($login);
            //     echo "<pre>";
            if ($login) { //create session...
                $data1 = array();
                $data1['id'] = $login->id;
                $data1['name'] = $login->first_name;
                $data1['email'] = $this->input->post('email');
                $data1['password'] = $this->input->post('password');
                $data1['user_type'] = $login->user_type;
                // echo "<pre>";
                //     print_r($data1);
                // echo "<pre>";
                $this->session->set_userdata($data1);
                if (!empty($this->input->post("remember"))) {
                    setcookie("loginId", $data1['email'], time() + (365 * 24 * 60 * 60));
                    setcookie("loginPass", $data1['password'],  time() + (365 * 24 * 60 * 60));
                } else {
                    setcookie("loginId", "");
                    setcookie("loginPass", "");
                }
                if (isset($_COOKIE['logincount'])) {
                    setcookie('logincount', $_COOKIE['logincount'] + 1, time() + (60 * 60 * 24 * 30));
                } else {
                    setcookie('logincount', 1, time() + (60 * 60 * 24 * 30));
                }
                if ($login['user_type'] == 'admin') {
                    redirect(base_url('Users/admin'));
                } else {
                    redirect(base_url('Info'));
                }
            } else {
                $this->session->set_flashdata('fail_login', 'Invalid Credentials.');
                return redirect('Users');
            }
        }
    }
    public function admin()
    {
        $data = array();
        $data['email'] = $this->session->userdata['email'];
        $data['RK'] = $this->Query->aview();
        $this->load->view('adminview', $data);
    }

    public function lout()
    {
        $this->session->sess_destroy();
        redirect(base_url('Users'));
    }

    //for registration into user table "signing in".
    public function signup($id = 0)
    {
        //used for adminpanel
        $data = array();
        $data['id'] = "";
        $data['first_name'] = "";
        $data['last_name'] = "";
        $data['email'] = "";
        $data['password'] = "";

        $data1 = array();
        $data1['upu'] = $data; //upu=update user..
        if ($id == 0) {
            $this->load->view('signup', $data1);
        } else {
            $this->load->model('Query');
            $member['up'] = $this->Query->euser($id); //up=update..
            // print_r($member['up']);
            $this->load->view('signup', $member);
        }
    }
    public function insert()
    {
        $data = array();
        $data['id'] = $this->input->post('id');
        $data['first_name'] = $this->input->post('firstname');
        $data['last_name'] = $this->input->post('lastname');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $this->load->model('Query');
        $this->Query->saveData($data);
        $this->session->set_flashdata('insert', 'Registerd Successfully.');
        redirect(base_url() . 'Users/signup');
    }
    private function image()
    {
        if (isset($_FILES['img'])) {
            $uploaddir = 'uploads/' . uniqid(rand());
            $uploadfile = $uploaddir . basename($_FILES['img']['name']);

            if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                return $uploadfile;
                echo "File is valid, and was successfully uploaded.\n";
            } else {
                echo "Possible file upload attack!\n";
            }
        }
    }

    //deletes info/dashboard/adminview particular data...
    public function delete($id)
    {
        $this->load->model('Query');
        $this->Query->delete($id);
        $data = array();
        $data['email'] = $this->session->userdata['email'];
        $data['password'] = $this->session->userdata['password'];
        $data = $this->Query->login($data);
        if ($data['user_type'] == 'admin') {
            redirect(base_url('Users/admin'));
        } else {
            redirect(base_url() . 'info/index');
        }
    }
}
