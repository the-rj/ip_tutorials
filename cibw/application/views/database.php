<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Winners</title>
    <link rel="icon" href="<?=base_url();?>assets/win/csst/icon.JPG" type="image/x-icon">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/my.css" type="text/css" media="all">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?=base_url();?>assets/win/js/jquery-3.4.1.min.js" type="text/javascript"></script>
</head>

<body style='background-color:black'>
    <?php include_once("nav.php"); ?>
    <br>
    <h1 class="text-center text-warning"><a onClick="history.go(-1);">Football Winners</a></h1>
    <br>
    <div class="container text-white border-success mb-3">
        <?php 
            // echo "<pre>";
            //     //print_r($userdetail);
            // echo "</pre>";
        ?>
        <font size="2">
        <table border="2" width = "100%" class="table head table-hover">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Category</th>
                    <th>Rank</th>
                    <th>Winnername</th>
                    <th>Passno</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php
                $count=0;
                foreach ($h->result() as $row) 
                {
                   ?>
						<tr>
                            <td><?php echo $count +=1;?></td>
                            <td><?=$row->category;?></td>
                            <td><?=$row->rank;?></td>
                            <td><?=$row->winnername;?></td>
                            <td><?=$row->passno;?></td>
                            <td><?=$row->date;?></td>
                            <td><?=$row->status;?></td>
						<td>
                        <h5><a class="badge badge-danger" href="<?=base_url('Player/delete/').$row->id?>" onClick="return confirm('Do you want to delete?');">Delete</a></h5>
						</td>
						<?php
                        echo '</tr>';
                    }
					
			?>
            </thead>
        </table>
                </font>

<script type="text/javascript">
        $("document").ready(function() {
            $(".btn-danger").click(function() {
                $(this).parents("tr").fadeOut(1000);
            });
        });
</script>
</body>
</html>