<?php
class Marksheet extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check login status
        if (!isset($this->session->userdata['email'])){
            redirect(base_url('Result'));
        }
        $this->load->Model('Query');
    }
    public function index()
    {
        // $this->load->database();  
        $data=array();
        $data['email']=$this->session->userdata['email'];
        $data['RK']=$this->Query->grade($data);
        $this->load->view('logout',$data);
    }
    
    public function score($id=0)
    {
        $data=array();
        $data['id']="";
        $data['en_no']="";
        $data['fullname'] = "";
        $data['email'] = "";
        $data['field'] = "";
        $data['semester'] = "";
        $data['SGPA'] = "";
        $data['CGPA'] = "";
        $data['image'] = "";

        $data1=array();
        $data1['m']=$data;
        if($id==0){
            $this->load->view('score',$data1);
        }
        else{
            $this->load->model('Query');
            $member['m'] = $this->Query->edit($id);
            // print_r($member['J']);
            $this->load->view('score',$member);
        }
    }
    public function save()
    {
        $data=array();
        $data['id']=$this->input->post('id');
        $data['en_no'] = $this->input->post('uid');
        $data['fullname'] = $this->input->post('fullname');
        $data['email'] = $this->input->post('email');
        $data['field'] = $this->input->post('btech');
        $data['semester'] = $this->input->post('sem');
        $data['SGPA'] = $this->input->post('sgpa');
        $data['CGPA'] = $this->input->post('cgpa');
        $data['image'] = $this->image();
        $this->load->model('Query');   
        $this->Query->markData($data);
        $this->session->set_flashdata('entered', 'Data Successfully Inserted.');
        redirect(base_url().'Marksheet/score');
    }

    private function image()
    {
        if(isset($_FILES['image']))
            {
                $uploaddir = 'uploads/'.uniqid(rand());
                $uploadfile = $uploaddir . basename($_FILES['image']['name']);
            
                    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                return $uploadfile;
                        echo "File is valid, and was successfully uploaded.\n";
                    } else {
                        echo "Possible file upload attack!\n";
            }
        }
    }

}
?>