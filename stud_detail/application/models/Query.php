<?php  
   class Query extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function login($data)  
      {  
         //data is retrive from this query  
         $query=$this->db->select('*')->from('users')->where(['email'=>$data['email'],'password'=>$data['password']])->get();  
         return $query->result_array()[0];        
      }
      public function saveData($data)
      {
         if($data['id']==0)
         {
            $this->db->insert('users',$data);
         }else{
            $this->db->where('id', $data['id']);
            $this->db->update('users',$data);
         }
      }
      public function grade($data)
      {
         $query=$this->db->SELECT('info.phone,info.image,info.id,info.email,info.en_no, info.fullname, info.year, info.field')->FROM('info')->JOIN('users','users.email=info.email')->WHERE('info.email',$data['email'])->get();
         if($query->num_rows()>0)
          return $query->result_array();
         else{
            $error['status']="Record Not Found";
            return $error;
         }
      }
      
      public function delete($id)
      {
         $this->db->where('id',$id)->delete('info');
      }
      public function markData($data)
      {
         if($data['id']==0){
            $this->db->insert('info',$data);
         }else{
            $this->db->where('id', $data['id']);
            $this->db->update('info',$data);
         }
      }
      public function edit($id)
      {
         $this->db->where('id',$id);
         return $member=$this->db->get('info')->result_array()[0];
      }

      //Admin panel
      public function aview()
      {
         $query=$this->db->get('info');
         return $query->result_array();
      }


   }  
?>