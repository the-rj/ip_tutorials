<!DOCTYPE html>
<html>

<head>
  <title>Register here!!</title>
  <link rel="icon" href="<?= base_url(); ?>assets/icon.PNG" type="image/x-icon">
  <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="<?= base_url(); ?>assets/js/validu.js"></script>
</head>

<body>
  <br>
  <!-- <?php
        echo "<pre>";
        print_r($J);
        echo "</pre>";
        ?> -->
  <div class="container">
    <form name="registration" id="registration" method="post" action="<?= base_url('Clubuv/save') ?>" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo set_value('id', $J['id']) ?>" />
      <!-- <fieldset> -->
        <legend align="center">Member Form</legend>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Fullname</label>
          <input type="text" class="form-control" placeholder="Enter Your Full Name" id="inputDefault" name="fullname" value="<?php echo set_value('fullname', $J['fullname']); ?>">
        </div>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Firstname</label>
          <input type="text" class="form-control" placeholder="Enter Your FirstName" id="inputDefault" name="fname" value="<?php echo set_value('fname', $J['firstname']); ?>">
        </div>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Lastname</label>
          <input type="text" class="form-control" placeholder="Enter Your LastName" id="inputDefault" name="lname" value="<?php echo set_value('lname', $J['lastname']); ?>">
        </div>
        <div class="form-group">
          <label for="user">Email address</label>
          <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo set_value('email', $J['email']);?>">
          <span id="usererr" class="hidden"></span>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <fieldset>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass" value="<?php echo set_value('pass', $J['password']); ?>">
        </div>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Contact</label>
          <input type="number" class="form-control" placeholder="Enter Your Number" id="inputDefault" name="phone" value="<?php echo set_value('phone', $J['contact']); ?>">
        </div>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Birthday</label>
          <input type="date" class="form-control" placeholder="Enter Your LastName" id="inputDefault" name="bdate" value="<?php echo set_value('bdate', $J['birthday']); ?>">
        </div>
        <div class="form-group">
          <label class="col-form-label" for="inputDefault">Country</label>
          <select class="form-control" id="country" name="con" value="">
            <option>Select Your Country</option>
            <option value="Australia" <?php if ($J['country'] == "Australia") echo "selected"; ?>>Australia</option>
            <option value="Brazil" <?php if ($J['country'] == "Brazil") echo "selected"; ?>>Brazil</option>
            <option value="Canada" <?php if ($J['country'] == "Canada") echo "selected"; ?>>Canada</option>
            <option value="Egypt" <?php if ($J['country'] == "Egypt") echo "selected"; ?>>Egypt</option>
            <option value="France" <?php if ($J['country'] == "France") echo "selected"; ?>>France</option>
            <option value="Greenland (Denmark)" <?php if ($J['country'] == "Greenland (Denmark)") echo "selected"; ?>>Greenland (Denmark)</option>
            <option value="India" <?php if ($J['country'] == "India") echo "selected"; ?>>India</option>
            <option value="Kazakhstan" <?php if ($J['country'] == "Kazakhstan") echo "selected"; ?>>Kazakhstan</option>
            <option value="Russia" <?php if ($J['country'] == "Russia") echo "selected"; ?>>Russia</option>
            <option value="United States of America" <?php if ($J['country'] == "United States of America") echo "selected"; ?>>United States of America</option>
            <option value="United Kingdom" <?php if ($J['country'] == "United Kingdom") echo "selected"; ?>>United Kingdom</option>
          </select>
        </div>
        <fieldset class="form-group">
          <legend>Gender</legend>
          <div class="form-group">
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="M" <?php if ($J['gender'] == 'M') echo "checked"; ?>>
              <label class="custom-control-label" for="customRadio1">Male</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" value="F" <?php if ($J['gender'] == 'F') echo "checked"; ?>>
              <label class="custom-control-label" for="customRadio2">Female</label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-form-label" for="inputDefault">Pass No.</label>
            <input type="text" class="form-control" placeholder="Enter Your Pass Number" id="inputDefault" name="pn" value="<?php echo set_value('pn', $J['passno']); ?>">
          </div>
          <div class="form-group">
            <label class="col-form-label" for="inputDefault">RegisterBy</label>
            <input type="text" class="form-control" placeholder="Type here.." id="inputDefault" name="rby" value="<?php echo set_value('rby', $J['registerby']); ?>">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <div class="input-group mb-3">
              <div class="custom-file">
                <?php echo form_upload('img'); ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-form-label" for="inputDefault">Status</label>
            <input type="text" class="form-control" placeholder="Y(yes) or N(no)" id="inputDefault" name="sat" value="<?php echo set_value('sat', $J['status']); ?>">
          </div>

          <button type="submit" class="btn btn-primary" value="upload">Submit</button>
          <?php echo anchor('Clubuv/back', 'Cancel', ['class' => 'btn btn-warning']); ?>
        </fieldset>
      </fieldset>
    </form>
  </div>
  <!-- Java Script -->
  <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
  <!-- Java Script -->
</body>

</html>