-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2020 at 03:34 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ip_tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `UsernameEmail` varchar(30) NOT NULL,
  `Password` varchar(16) NOT NULL,
  `MobileNumber` bigint(10) NOT NULL,
  `Birthdate` date NOT NULL,
  `Country` varchar(20) NOT NULL,
  `State` varchar(20) NOT NULL,
  `City` varchar(20) NOT NULL,
  `Image` text NOT NULL,
  `Note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`ID`, `Name`, `UsernameEmail`, `Password`, `MobileNumber`, `Birthdate`, `Country`, `State`, `City`, `Image`, `Note`) VALUES
(1, 'Rahul Jagetia', 'rjagetiya780@rku.ac.in', '********', 8347383659, '1999-05-01', 'Australia', 'Gujarat', 'Jamnagar', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_IMG_0111.JPG', 'RKU Student'),
(7, 'Manthan Kaneriya', 'kaneriyamanthan@gmail.com', '12345678', 9106659856, '2000-08-29', 'India', 'Gujarat', 'Rajkot', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_HP Envy G wallpaper.jpg', 'RKU Student'),
(8, 'Parth Agravat', 'pagravat301@rku.ac.in', '12345678', 9067093235, '2000-02-20', 'India', 'Gujarat', 'Rajkot', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_HP Envy wallpaper.jpg', 'RKU Student'),
(9, 'Deepak shukla', 'dshukla293@rku.ac.in', '12345678', 9898998877, '2000-06-08', 'India', 'Gujarat', 'Rajkot', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_IMG_6027.JPG', 'RKU Student'),
(16, 'Gautam Kodiyatar', 'gautam.kodiyatar@rku.ac.in', '12345678', 9925103711, '2000-08-10', 'India', 'Gujarat', 'Porbandar', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_g.jpg', 'Murlidhar Student'),
(22, 'dharmil dhameliya', 'dshukla293@rku.ac.in', '1e234535645', 7934081544, '2020-03-11', 'India', 'Gujarat', 'Surat', 'uploads/img/nnb58gbqogoetl562ab34ncvg5_HP Envy G wallpaper.jpg', 'RKU Student');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `en_no` varchar(12) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `field` varchar(40) NOT NULL,
  `semester` varchar(7) NOT NULL,
  `SGPA` float NOT NULL,
  `CGPA` float NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `email`, `en_no`, `fullname`, `field`, `semester`, `SGPA`, `CGPA`, `created_on`) VALUES
(1, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'First', 8.75, 8.75, '2020-03-27 07:10:04'),
(2, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Second', 9, 8.88, '2020-03-27 07:38:39'),
(3, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Third', 6.21, 7.99, '2020-03-27 07:38:39'),
(9, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Fourth', 0, 0, '2020-03-27 07:39:16'),
(10, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Fifth', 0, 0, '2020-03-27 07:39:16'),
(11, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Sixth', 0, 0, '2020-03-27 07:39:16'),
(12, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Seventh', 0, 0, '2020-03-27 07:39:16'),
(13, 'rjagetiya780@rku.ac.in', '18SOEIT11009', 'Jagetia Rahul Rajesh', 'Information Technology', 'Eighth', 0, 0, '2020-03-27 07:39:39'),
(14, 'pkakadiya274@rku.ac.in ', '18SOEIT11012', 'Pranav Kakadiya', 'Information Technology', 'First', 8, 8, '2020-03-27 09:18:21'),
(15, 'pkakadiya274@rku.ac.in ', '18SOEIT11012', 'Pranav Kakadiya', 'Information Technology', 'Second', 9, 8.5, '2020-03-27 09:18:47'),
(16, 'pkakadiya274@rku.ac.in ', '18SOEIT11012', 'Pranav Kakadiya', 'Information Technology', 'Third', 9, 8.67, '2020-03-27 09:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_photo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `profile_photo`, `created_at`) VALUES
(1, 'Rahul', 'Jagetia', 'rjagetiya780@rku.ac.in', 'rahulrj', NULL, '2020-03-27 10:46:36'),
(3, 'Pranav', 'Kakadiya', 'pkakadiya274@rku.ac.in ', '123456789', NULL, '2020-03-27 08:43:00'),
(4, 'Deepak', 'Shukla', 'dshukla293@rku.ac.in', 'dshukla', NULL, '2020-03-27 08:43:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
