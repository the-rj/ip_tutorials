<?php  
   class Select2 extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function add()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('winners');  
         return $query;  
      }  
      public function saveData($data)
      {
         $this->db->insert('winners',$data);
      }
      public function duser($id)
      {
         $this->db->where('ID',$id)->delete('winners');
      }

   }  
?>